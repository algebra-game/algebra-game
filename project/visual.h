#include "Global.h"
#include "GuiGen.h"
#include "algebra.h"
#include "Maze.h"

typedef CameraControl<Index> MyCamera;
typedef GraphicalInterface<Index> Graphic;
typedef SoundInterface<Index> SoundManager;

struct PowerWord
{
    std::string sWord;
    std::string sPower;

    PowerWord(std::string sWord_, std::string sPower_)
        :sWord(sWord_), sPower(sPower_){}

    bool IsPower(int n)
    {
        if(n > 0 && n < sPower.size())
            return sPower[n] != ' ';
        return false;
    }
};

struct PowerFont
{
    SP<FontWriter> pFont;
    SP<FontWriter> pSmallFont;

    Point pPowerGap;
    int nGap;

    void DrawColorWord(PowerWord s, Point p, Color c);
};

struct Grid
{
    Size sz;
    Point pOffset;
    Point pGap;

    Point Wrap(Point p);
    Point Get(Point p);
};

void AdjustOffset(MyCamera cc, Grid& g);

struct WallDescription
{
    std::vector<bool> vWalls;

    WallDescription():vWalls(4, false){}
};

struct ScreenObject
{
    Point p;
    Point pStr;

    Index iImg;
    std::string sText;
    int nGrpEl;
    
    WallDescription wd;
};



Index GetWallImage(SP<Graphic> pGr, MyPreloader& pr, WallDescription& wd);


class AlgebraGameGlobalController: public GlobalController
{
    SP<Graphic> pGr;
    SP< SoundManager > pSndMng;
    SP<MessageWriter> pMsg;

    FilePath fp;
    SP<MyPreloader> pPr;

    SP<FontWriter> pFont;
    SP<FontWriter> pSmallFont;

    MyCamera cc;

    PowerFont pf;
    Grid g;
    std::vector<ScreenObject> vObj;

    Point pSel;

    Group grp;

    ScreenObject* GetScreenObj(Point p);
    float fTrans;


public:

    AlgebraGameGlobalController(ProgramEngine pe);
    ~AlgebraGameGlobalController(){}

    /*virtual*/ void Update();
    
    /*virtual*/ void KeyDown(GuiKeyType nCode);
    /*virtual*/ void KeyUp(GuiKeyType nCode){}
};