#ifndef MAZE_ALREADY_INCLUDED_MAZE_0816
#define MAZE_ALREADY_INCLUDED_MAZE_0816

#include "GuiGen.h"
#include <map>
#include <cmath>

using namespace Gui;

template <class V, class O>
struct Statistician
{
    unsigned nObj;
    V vSum;
    V vSumSq;

    V vMin;
    O oMin;

    V vMax;
    O oMax;

    Statistician():nObj(0), vSum(0), vSumSq(0){}

    void Add(V val, O obj = O())
    {
        if(nObj == 0)
        {
            vMin = vMax = val;
            oMin = oMax = obj;
        }
        else
        {
            if(val < vMin)
            {
                vMin = val;
                oMin = obj;
            }

            if(val > vMax)
            {
                vMax = val;
                oMax = obj;
            }
        }

        ++nObj;

        vSum += val;
        vSumSq += val*val;
    }

    template<class R>
    R GetMean()
    {
        return R(vSum)/nObj;
    }
    
    template<class R>
    R GetSqMean()
    {
        return R(vSumSq)/nObj;
    }

    template<class R>
    R GetVariance()
    {
        R vMean = GetMean<<R>();
        return GetSqMean<R>() - vMean*vMean;
    }

    template<class R>
    R GetDeviation()
    {
        return std::sqrt(GetVariance<R>());
    }
};

template<class T> 
class Matrix
{
    Gui::Size sz;
    std::vector<T> v;
public:
    Matrix(Gui::Size sz_):sz(sz_), v(sz_.x * sz_.y){}

    T& operator [](Gui::Point p){return v[p.x + p.y * sz.x];}
    T& at(Gui::Point p){return v.at(p.x + p.y * sz.x);}

    Gui::Size GetSize(){return sz;}
};

struct Spot;
struct Link;
struct Pass;

struct Spot
{
    std::vector<Link> vLinks;

    int nState;

    Spot():nState(0){}
};

struct Link
{
    Pass* pPass;
    Spot* pTo;

    Link():pPass(0), pTo(0){}
};

struct Pass
{
    bool bWalled;
    int nWeight;

    Pass(bool bWalled_, int nWeight_ = 1):bWalled(bWalled_), nWeight(nWeight_){}
};

struct Maze
{
    std::vector<Spot*> vSpots;

    void Flush(int n = 0);
    void Wipe(bool bWall = true);
};

struct SquareMaze
{
    std::vector<Pass*> vPasses;
    Matrix<Spot> mtxSpots;

    SquareMaze(Gui::Size sz);

    void GetMaze(Maze& m);

    ~SquareMaze()
    {
        for(unsigned i = 0; i < vPasses.size(); ++i)
            delete vPasses[i];
    }
};


void Draw(SquareMaze& sq, SP< GraphicalInterface<Index> > pGr, Gui::Point pOffset, Gui::Size szElement, bool bSt = false, bool bPar = false);

struct CubeMaze
{
    std::vector<Pass*> vPasses;
    std::vector<Matrix<Spot> > cbSpots;
    Gui::Size sz;

    CubeMaze(Gui::Size sz_, int nHeight);

    void GetMaze(Maze& m);

    ~CubeMaze()
    {
        for(unsigned i = 0; i < vPasses.size(); ++i)
            delete vPasses[i];
    }
};

void Draw(CubeMaze& sq, SP< GraphicalInterface<Index> > pGr, Gui::Point pOffset, Gui::Size szElement, int nLvl);

struct Wall
{
    Spot* pSpot;
    Link l;
};

bool IsIsolated(Spot* pSpot);

Spot* AldousBroderMazeStep(Spot*& pSpot);
void AldousBroderMaze(Maze& mz);

bool WilsonMazeStep(Maze& mz, Spot* pInitSpot);
void WilsonMaze(Maze& mz);

Spot* RecursiveMazeStep(std::list<Spot*>& vStack);
void RecursiveMaze(Maze& mz);

void PrimMazeInit(std::vector<Spot*>& vFrontiers, Spot* pInitSpot);
void PrimMazeInit(std::vector<Spot*>& vFrontiers, Maze& mz);
Spot* PrimMazeStep(std::vector<Spot*>& vFrontiers, bool bWeigh = false);
void FrontierAdjust(std::vector<Spot*>& vFrontiers, Spot* pNewSpot);
void PrimMaze(Maze& mz, bool bWeigh = false);

void KruskalMazeInit(Maze& mz, std::vector<Wall>& vWalls, bool bWeigh = false);
bool KruskalMazeStep(Maze& mz, std::vector<Wall>& vWalls);
void KruskalMaze(Maze& mz, bool bWeigh = false);

Spot* HuntKillMazeStep(Maze& mz, Spot*& pSpot);
void HuntKillMaze(Maze& mz);

Spot* GrowingTreeMazeStep(std::vector<Spot*>& vStack, int nDepth = 10);
void GrowingTreeMaze(Maze& mz, int nDepth = 10);

void DivideMazeInit(SquareMaze& sq, std::list<Gui::Rectangle>& v);
bool DivideMazeStep(SquareMaze& sq, std::list<Gui::Rectangle>& v);
void DivideMaze(SquareMaze& sq);

void EllerMazeInit(SquareMaze& sq);
bool EllerMazeStep(SquareMaze& sq, int& nY);
void EllerMaze(SquareMaze& sq);

void BinaryMaze(SquareMaze& sq);

bool SidewinderMazeStep(SquareMaze& sq, int& nY);
void SidewinderMaze(SquareMaze& sq);

void SimpleBraidMazeInit(Maze& mz, std::vector<Wall>& vWalls, bool bWeigh = false);
void SimpleBraidMazeCompletionInit(Maze& mz, std::vector<Wall>& vWalls, bool bWeigh = false);
bool SimpleBraidMazeStep(std::vector<Wall>& vWalls);
void SimpleBraidMaze(Maze& mz, bool bWeigh = false);
void SimpleBraidMazeCompletion(Maze& mz, bool bWeigh = false);

bool RecursiveOpenMazeStep(std::list<Spot*>& vStack);

void DeadEndRemoverInit(Maze& mz, std::vector<Spot*>& vDead, float fRemove = 1.0F);
bool DeadEndRemoverStep(std::vector<Spot*>& vDead);
void DeadEndRemover(Maze& mz, float fRemove = 1.0F);

bool DeadEndRemoverTameStep(std::vector<Spot*>& vDead, bool bMin, int nMaxPath);
void DeadEndRemoverTame(Maze& mz, float fRemove, bool bMin, int nMaxPath);

void BlumpsRemover(SquareMaze& sq, bool bBraid = true);

int DeadEndCounter(Maze& mz);
float River(Maze& mz);

struct SpotPair
{
    Spot* p1;
    Spot* p2;

    int n;

    SpotPair():p1(0), p2(0){}
    SpotPair(Spot* p1_, Spot* p2_):p1(p1_), p2(p2_){}
};

SpotPair MaxDistance(Maze& mz, Maze& mzSeeds);

bool FindPath(SpotPair sp, Maze& mz);

int Loopiness(Maze& mz);

int MaxiLooper(Maze& mz, unsigned nSample, int nRuns);
int MaxiLooper(Maze& mz, int nRuns);

void CarveMazeSpace(Maze& mz, bool bCleanUp = false);
void LimitPartialMaze(Maze& mz);



#endif // MAZE_ALREADY_INCLUDED_MAZE_0816