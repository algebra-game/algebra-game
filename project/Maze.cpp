#include "Maze.h"

#include <iostream>
#include <set>

void Maze::Flush(int n)
{
    for(unsigned j = 0; j < vSpots.size(); ++j)
        vSpots[j]->nState = n;
}

void Maze::Wipe(bool bWall)
{
    Flush();

    for(unsigned j = 0; j < vSpots.size(); ++j)
        for(unsigned i = 0; i < vSpots[j]->vLinks.size(); ++i)
            if(vSpots[j]->vLinks[i].pPass)
                vSpots[j]->vLinks[i].pPass->bWalled = bWall;
}





SquareMaze::SquareMaze(Size sz):mtxSpots(sz)
{
    Point p;
    for(p.x = 0; p.x < mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mtxSpots.GetSize().y; ++p.y)
    {
        mtxSpots[p].vLinks = std::vector<Link>(4);
    }

    for(p.x = 0; p.x < mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mtxSpots.GetSize().y; ++p.y)
    {
        if(p.x != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x - 1, p.y);
            
            Pass* pPass = new Pass(true, 1);
            vPasses.push_back(pPass);
            
            mtxSpots[pCurr].vLinks[0].pPass = pPass;
            mtxSpots[pCurr].vLinks[0].pTo   = &(mtxSpots[pPrev]);

            mtxSpots[pPrev].vLinks[1].pPass = pPass;
            mtxSpots[pPrev].vLinks[1].pTo   = &(mtxSpots[pCurr]);

        }

        if(p.y != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x, p.y - 1);
            
            Pass* pPass = new Pass(true, 1);
            vPasses.push_back(pPass);
            
            mtxSpots[pCurr].vLinks[2].pPass = pPass;
            mtxSpots[pCurr].vLinks[2].pTo   = &(mtxSpots[pPrev]);

            mtxSpots[pPrev].vLinks[3].pPass = pPass;
            mtxSpots[pPrev].vLinks[3].pTo   = &(mtxSpots[pCurr]);

        }
    }
}

void SquareMaze::GetMaze(Maze& m)
{
    m = Maze();

    Point p;
    for(p.x = 0; p.x < mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mtxSpots.GetSize().y; ++p.y)
        m.vSpots.push_back( &(mtxSpots[p]) );
}


bool IsWalled(Pass* pPass)
{
    return (!pPass) || pPass->bWalled;
}

void Draw(SquareMaze& sq, SP< GraphicalInterface<Index> > pGr, Point pOffset, Size szElement, bool bSt, bool bPar)
{
    Size sz = szElement;
    
    Point pi;
    for(pi.x = 0; pi.x < sq.mtxSpots.GetSize().x; ++pi.x)
    for(pi.y = 0; pi.y < sq.mtxSpots.GetSize().y; ++pi.y)
    {
        Point p = pOffset + Point(pi.x * sz.x, pi.y * sz.y);

        if(bPar && IsIsolated(&sq.mtxSpots[pi]))
            continue;
        
        if(IsWalled(sq.mtxSpots[pi].vLinks[0].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.mtxSpots[pi].vLinks[1].pPass))
            pGr->DrawRectangle(Rectangle(p.x + sz.x, p.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.mtxSpots[pi].vLinks[2].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + sz.x + 1, p.y + 1), Color(), false);
        if(IsWalled(sq.mtxSpots[pi].vLinks[3].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y + sz.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);

        if(bSt && (sq.mtxSpots[pi].nState == 1))
            pGr->DrawRectangle(Rectangle(p.x + sz.x/3, p.y + sz.y/3, p.x + sz.x*2/3 + 1, p.y + sz.y*2/3 + 1), Color(), false);
    }
}

CubeMaze::CubeMaze(Size sz_, int nHeight): cbSpots(nHeight, Matrix<Spot>(sz_)), sz(sz_)
{
    Point p; unsigned nH;
    for(nH = 0; nH < cbSpots.size(); ++nH)
    for(p.x = 0; p.x < sz.x; ++p.x)
    for(p.y = 0; p.y < sz.y; ++p.y)
        cbSpots[nH][p].vLinks = std::vector<Link>(6);
    
    for(nH = 0; nH < cbSpots.size(); ++nH)
    for(p.x = 0; p.x < sz.x; ++p.x)
    for(p.y = 0; p.y < sz.y; ++p.y)
    {
        if(p.x != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x - 1, p.y);
            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            cbSpots[nH][pCurr].vLinks[0].pPass = pPass;
            cbSpots[nH][pCurr].vLinks[0].pTo   = &(cbSpots[nH][pPrev]);

            cbSpots[nH][pPrev].vLinks[1].pPass = pPass;
            cbSpots[nH][pPrev].vLinks[1].pTo   = &(cbSpots[nH][pCurr]);

        }

        if(p.y != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x, p.y - 1);
            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            cbSpots[nH][pCurr].vLinks[2].pPass = pPass;
            cbSpots[nH][pCurr].vLinks[2].pTo   = &(cbSpots[nH][pPrev]);

            cbSpots[nH][pPrev].vLinks[3].pPass = pPass;
            cbSpots[nH][pPrev].vLinks[3].pTo   = &(cbSpots[nH][pCurr]);

        }

        if(nH != 0)
        {            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            cbSpots[nH][p].vLinks[4].pPass = pPass;
            cbSpots[nH][p].vLinks[4].pTo   = &(cbSpots[nH - 1][p]);

            cbSpots[nH - 1][p].vLinks[5].pPass = pPass;
            cbSpots[nH - 1][p].vLinks[5].pTo   = &(cbSpots[nH][p]);

        }
    }
}

void CubeMaze::GetMaze(Maze& m)
{
    m = Maze();

    Point p; unsigned nH;
    for(nH = 0; nH < cbSpots.size(); ++nH)
    for(p.x = 0; p.x < sz.x; ++p.x)
    for(p.y = 0; p.y < sz.y; ++p.y)
        m.vSpots.push_back( &(cbSpots[nH][p]) );
}

void Draw(CubeMaze& sq, SP< GraphicalInterface<Index> > pGr, Point pOffset, Size szElement, int nLvl)
{
    Size sz = szElement;
    
    Point pi;
    for(pi.x = 0; pi.x < sq.sz.x; ++pi.x)
    for(pi.y = 0; pi.y < sq.sz.y; ++pi.y)
    {
        Point p = pOffset + Point(pi.x * sz.x, pi.y * sz.y);

        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[0].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[1].pPass))
            pGr->DrawRectangle(Rectangle(p.x + sz.x, p.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[2].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + sz.x + 1, p.y + 1), Color(), false);
        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[3].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y + sz.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
        if(!IsWalled(sq.cbSpots[nLvl][pi].vLinks[5].pPass))
            pGr->DrawRectangle(Rectangle(p.x + sz.x/4, p.y + sz.y/4, p.x + sz.x*3/4, p.y + sz.y*3/4), Color(), false);
        if(!IsWalled(sq.cbSpots[nLvl][pi].vLinks[4].pPass))
            pGr->DrawRectangle(Rectangle(Point(p.x + sz.x/2, p.y + sz.y/2), Size(2,2)), Color(255,0,0), false);
    }
}

int RandomWalk(const std::vector<Link>& vLnkIns)
{
    unsigned i, sz = vLnkIns.size();
    int nWeight = 0, nWeightTotal = 0;
    
    for(i = 0; i < sz; ++i)
        if(vLnkIns[i].pPass)
            nWeightTotal += vLnkIns[i].pPass->nWeight;
    
    int n;
    if(nWeightTotal > 10000)
        n = int(double(rand())/(RAND_MAX+1)*nWeightTotal);
    else
        n = rand()%nWeightTotal;

    for(i = 0; i < sz; ++i)
        if(vLnkIns[i].pPass)
            if((nWeight += vLnkIns[i].pPass->nWeight) > n)
                return i;
    return -1;
}



bool IsIsolated(Spot* pSpot)
{
    if(!pSpot)
        return true;
    
    unsigned j;
    for(j = 0; j < pSpot->vLinks.size(); ++j)
        if(!IsWalled(pSpot->vLinks[j].pPass))
            break;
    return j == pSpot->vLinks.size();
}

int Distance(Spot* pSpotOne, Spot* pSpotTwo)
{
    int nRet = -1;
    
    if(pSpotOne == pSpotTwo)
        return 0;

    std::list<Spot*> lsPath, lsAll;
    lsPath.push_back(pSpotOne);
    lsAll.push_back(pSpotOne);

    while(lsPath.size())
    {
        Spot* pCurrSpot = lsPath.front();
        lsPath.pop_front();

        for(size_t i = 0, sz = pCurrSpot->vLinks.size(); i < sz; ++i)
            if(pCurrSpot->vLinks[i].pPass && !pCurrSpot->vLinks[i].pPass->bWalled)
                if(pCurrSpot->vLinks[i].pTo->nState == 0)
                {
                    if(pCurrSpot->vLinks[i].pTo == pSpotTwo)
                    {
                        nRet = pCurrSpot->nState + 1;
                        goto cleanup;
                    }
                    pCurrSpot->vLinks[i].pTo->nState = pCurrSpot->nState + 1;
                    lsPath.push_back(pCurrSpot->vLinks[i].pTo);
                    lsAll.push_back(pCurrSpot->vLinks[i].pTo);
                }
    }

cleanup:

    while(lsAll.size())
    {
        lsAll.front()->nState = 0;
        lsAll.pop_front();
    }

    return nRet;
}


Spot* AldousBroderMazeStep(Spot*& pSpot)
{
    Link* pLink = &(pSpot->vLinks[RandomWalk(pSpot->vLinks)]);
    Spot* pNewSpot = pLink->pTo;

    if(IsIsolated(pNewSpot))
        pLink->pPass->bWalled = false;

    pSpot = pNewSpot;

    return pSpot;
}


void AldousBroderMaze(Maze& mz)
{
    Spot* pSpot = mz.vSpots[rand()%mz.vSpots.size()];
    
    while(true)
    {
        unsigned i;
        for(i = 0; i < mz.vSpots.size(); ++i)
            if(IsIsolated(mz.vSpots[i]))
                break;

        if(i == mz.vSpots.size())
            return;

        AldousBroderMazeStep(pSpot);
    }
}

bool WilsonMazeStep(Maze& mz, Spot* pInitSpot)
{
    std::vector<Spot*> vIsolated;
    
    unsigned i;
    for(i = 0; i < mz.vSpots.size(); ++i)
    {
        if(mz.vSpots[i] == pInitSpot)
            continue;
        
        if(IsIsolated(mz.vSpots[i]))
            vIsolated.push_back(mz.vSpots[i]);
    }

    if(vIsolated.empty())
        return false;

    Spot* pCurrSpot = vIsolated[rand()%vIsolated.size()];
    Spot* pStarSpot = pCurrSpot;
    std::vector<int> vPath;

    while(true)
    {
        int n = RandomWalk(pCurrSpot->vLinks);
        vPath.push_back(n);
        pCurrSpot = pCurrSpot->vLinks[n].pTo;

        if(pCurrSpot == pInitSpot || !IsIsolated(pCurrSpot))
            break;
    }

    pCurrSpot = pStarSpot;
    

    for(i = 0; i < vPath.size(); ++i)
    {
        Spot* pNewSpot = pCurrSpot->vLinks[vPath[i]].pTo;
        if(IsIsolated(pNewSpot) || i == vPath.size() - 1)
            pCurrSpot->vLinks[vPath[i]].pPass->bWalled = false;
        pCurrSpot = pNewSpot;
    }

    return true;
}

void WilsonMaze(Maze& mz)
{
    Spot* pInitSpot = mz.vSpots[rand()%mz.vSpots.size()];

    while(WilsonMazeStep(mz, pInitSpot));
}

Spot* RecursiveMazeStep(std::list<Spot*>& vStack)
{
    while(true)
    {
        if(vStack.empty())
            return 0;
        
        Spot* pSpot = vStack.back();

        std::vector<Link> vChoices;

        for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
            if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
                vChoices.push_back(pSpot->vLinks[i]);
        if(vChoices.empty())
        {
            vStack.pop_back();
            continue;
        }

        int n = RandomWalk(vChoices);

        vChoices[n].pPass->bWalled = false;
        vStack.push_back(vChoices[n].pTo);

        return vStack.back();
    }
}

void RecursiveMaze(Maze& mz)
{
    std::list<Spot*> vStack;
    vStack.push_back(mz.vSpots[rand()%mz.vSpots.size()]);

    while(RecursiveMazeStep(vStack));
}

Spot* PrimMazeStep(std::vector<Spot*>& vFrontiers, bool bWeigh)
{
    if(vFrontiers.empty())
        return 0;

    unsigned i;
    Spot* pSpot;
    
    if(!bWeigh)
    {
        int n = rand()%vFrontiers.size();
        pSpot = vFrontiers[n];
        vFrontiers.erase(vFrontiers.begin() + n);
        pSpot->nState = 2;

        std::vector<Link> vLnkIns;
        for(i = 0; i < pSpot->vLinks.size(); ++i)
            if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pTo->nState == 2)
                vLnkIns.push_back(pSpot->vLinks[i]);

        vLnkIns[rand()%vLnkIns.size()].pPass->bWalled = false;
    }
    else
    {
        std::vector<Link> vFrontierLinks;
        std::vector<int>  vFrontierIndex;
        for(unsigned j = 0; j < vFrontiers.size(); ++j)
        {
            pSpot = vFrontiers[j];
            for(i = 0; i < pSpot->vLinks.size(); ++i)
                if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pTo->nState == 2)
                {
                    vFrontierLinks.push_back(pSpot->vLinks[i]);
                    vFrontierIndex.push_back(j);
                }
        }

        int n = RandomWalk(vFrontierLinks);
        int k = vFrontierIndex[n];
        pSpot = vFrontiers[k];
        vFrontiers.erase(vFrontiers.begin() + k);
        pSpot->nState = 2;

        vFrontierLinks[n].pPass->bWalled = false;
    }
    
    FrontierAdjust(vFrontiers, pSpot);
    
    return pSpot;
}

void FrontierAdjust(std::vector<Spot*>& vFrontiers, Spot* pNewSpot)
{
    for(unsigned i = 0; i < pNewSpot->vLinks.size(); ++i)
        if(pNewSpot->vLinks[i].pTo && pNewSpot->vLinks[i].pTo->nState == 0)
        {
            pNewSpot->vLinks[i].pTo->nState = 1;
            vFrontiers.push_back(pNewSpot->vLinks[i].pTo);
        }
}


void PrimMazeInit(std::vector<Spot*>& vFrontiers, Spot* pInitSpot)
{
    vFrontiers.clear();
    pInitSpot->nState = 2;

    unsigned i;
    for(i = 0; i < pInitSpot->vLinks.size(); ++i)
        if(pInitSpot->vLinks[i].pTo)
        {
            pInitSpot->vLinks[i].pTo->nState = 1;
            vFrontiers.push_back(pInitSpot->vLinks[i].pTo);
        }

}

void PrimMazeInit(std::vector<Spot*>& vFrontiers, Maze& mz)
{
    vFrontiers.clear();
    mz.Flush(0);
    
    for(unsigned i = 0; i < mz.vSpots.size(); ++i)
    {
        if(!IsIsolated(mz.vSpots[i]))
            mz.vSpots[i]->nState = 2;
    }

    for(unsigned i = 0; i < mz.vSpots.size(); ++i)
    {
        Spot* pSpot = mz.vSpots[i];

        if(pSpot->nState != 0)
            continue;

        for(unsigned j = 0; j < pSpot->vLinks.size(); ++j)
            if(pSpot->vLinks[j].pTo && pSpot->vLinks[j].pTo->nState == 2)
            {
                pSpot->nState = 1;
                vFrontiers.push_back(pSpot);
                break;
            }
    }
}

void PrimMaze(Maze& mz, bool bWeigh)
{
    Spot* pInitSpot = mz.vSpots[rand()%mz.vSpots.size()];

    std::vector<Spot*> vFrontiers;

    PrimMazeInit(vFrontiers, pInitSpot);

    while(PrimMazeStep(vFrontiers, bWeigh));
}

template<class T>
void Shuffle(std::vector<T>& v)
{
    std::vector<T> vShuff;

    while(!v.empty())
    {
        int i = rand()%v.size();
        vShuff.push_back(v[i]);
        v.erase(v.begin() + i);
    }

    v = vShuff;
}

void ShuffleWeighed(std::vector<Wall>& v, bool bRevert = true)
{
    std::vector<Wall> vShuff;
    std::vector<Link> vLinks(v.size());

    for(unsigned i = 0; i < v.size(); ++i)
        vLinks[i] = v[i].l;

    while(!v.empty())
    {
        int n = RandomWalk(vLinks);
        vShuff.push_back(v[n]);
        
        v.erase(v.begin() + n);
        vLinks.erase(vLinks.begin() + n);
    }

    v.clear();

    
    if(bRevert)
        for(int i = int(vShuff.size()) - 1; i >= 0 ; --i)
            v.push_back(vShuff[i]);
    else
        for(unsigned i = 0; i < vShuff.size(); ++i)
            v.push_back(vShuff[i]);
}

void GetWalls(Maze& mz, std::vector<Wall>& vWalls)
{
    mz.Flush();

    Spot* pInitSpot = mz.vSpots.at(0);

    pInitSpot->nState = 1;

    std::list<Spot*> vFrontier;
    vFrontier.push_back(pInitSpot);

    while(!vFrontier.empty())
    {
        Spot* pSpot = vFrontier.back();
        vFrontier.pop_back();
        pSpot->nState = 2;

        for(unsigned j = 0; j < pSpot->vLinks.size(); ++j)
        {
            if(pSpot->vLinks[j].pTo && pSpot->vLinks[j].pTo->nState != 2)
            {
                Wall kw;
                kw.l = pSpot->vLinks[j];
                kw.pSpot = pSpot;
                vWalls.push_back(kw);

                if(pSpot->vLinks[j].pTo->nState == 0)
                {
                    pSpot->vLinks[j].pTo->nState = 1;
                    vFrontier.push_back(pSpot->vLinks[j].pTo);
                }
            }
        }
    }
}



void KruskalMazeInit(Maze& mz, std::vector<Wall>& vWalls, bool bWeigh)
{
    GetWalls(mz, vWalls);

    if(!bWeigh)
        Shuffle(vWalls);
    else
        ShuffleWeighed(vWalls);

    unsigned j;
    for(j = 0; j < mz.vSpots.size(); ++j)
        mz.vSpots[j]->nState = j;
}

bool KruskalMazeStep(Maze& mz, std::vector<Wall>& vWalls)
{
    if(vWalls.empty())
        return false;

    Wall kw = vWalls.back();
    vWalls.pop_back();

    if(kw.pSpot->nState == kw.l.pTo->nState)
        return true;
    std::list<Spot*> lsRecolor;
    lsRecolor.push_back(kw.l.pTo);
    while(!lsRecolor.empty())
    {
        Spot* pCurr = lsRecolor.back();
        lsRecolor.pop_back();
        pCurr->nState = kw.pSpot->nState;
        for(unsigned i = 0; i < pCurr->vLinks.size(); ++i)
            if(pCurr->vLinks[i].pPass && !pCurr->vLinks[i].pPass->bWalled)
            if(pCurr->vLinks[i].pTo->nState != kw.pSpot->nState)
                lsRecolor.push_back(pCurr->vLinks[i].pTo);
    }
    kw.l.pPass->bWalled = false;

    return true;
}


void KruskalMaze(Maze& mz, bool bWeigh)
{
    std::vector<Wall> vWalls;

    KruskalMazeInit(mz, vWalls, bWeigh);

    while(KruskalMazeStep(mz, vWalls));
}

Spot* HuntKillMazeStep(Maze& mz, Spot*& pSpot)
{
    unsigned i,j, sz;

    std::vector<Link> vChoices;

    for(i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
            vChoices.push_back(pSpot->vLinks[i]);

    if(!vChoices.empty())
    {
        int n = RandomWalk(vChoices);

        vChoices[n].pPass->bWalled = false;
        pSpot = vChoices[n].pTo;
        return pSpot;
    }

    std::vector<Link> vHuntLinks;

    for(i = 0; i < mz.vSpots.size(); ++i)
    {
        Spot* pHuntSpot = mz.vSpots[i];
        if(!IsIsolated(pHuntSpot))
            continue;
        for(j = 0; j < pHuntSpot->vLinks.size(); ++j)
            if(pHuntSpot->vLinks[j].pTo && !IsIsolated(pHuntSpot->vLinks[j].pTo))
            {
                Link l;
                l.pPass = pHuntSpot->vLinks[j].pPass;
                l.pTo = pHuntSpot;
                vHuntLinks.push_back(l);
            }
    }

    if(vHuntLinks.empty())
        return 0;
    
    int n = RandomWalk(vHuntLinks);
    pSpot = vHuntLinks[n].pTo;
    vHuntLinks[n].pPass->bWalled = false;
    return pSpot;
}

void HuntKillMaze(Maze& mz)
{
    Spot* pSpot = mz.vSpots[rand()%mz.vSpots.size()];
    
    while(HuntKillMazeStep(mz, pSpot));
}

Spot* GrowingTreeMazeStep(std::vector<Spot*>& vStack, int nDepth)
{
    bool bRandom = false;
    while(true)
    {
        if(vStack.empty())
            return 0;
        
        int n;
        if(rand()%nDepth == 0 || bRandom)
        {
            n = rand()%vStack.size();
            bRandom = true;
        }
        else
            n = vStack.size() - 1;
        Spot* pSpot = vStack[n];

        std::vector<Link> vChoices;

        for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
            if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
                vChoices.push_back(pSpot->vLinks[i]);

        if(vChoices.empty())
        {
            vStack.erase(vStack.begin() + n);
            continue;
        }

        n = RandomWalk(vChoices);

        vChoices[n].pPass->bWalled = false;
        vStack.push_back(vChoices[n].pTo);

        return vStack.back();
    }
}

void GrowingTreeMaze(Maze& mz, int nDepth)
{
    std::vector<Spot*> vStack;
    vStack.push_back(mz.vSpots[rand()%mz.vSpots.size()]);

    while(GrowingTreeMazeStep(vStack, nDepth));
}

void DivideMazeRec(SquareMaze& mz, Rectangle r, std::list<Rectangle>& v)
{
    if(r.sz.x == 1 || r.sz.y == 1)
        return;

    //if(float(rand())/RAND_MAX < float(r.sz.x) / (r.sz.y + r.sz.x))
    if(float(rand())/RAND_MAX < float(r.sz.y) / (r.sz.y + r.sz.x))
    //if(r.sz.y > r.sz.x)
    //if(rand()%2)
    {
        Point p;
        p.y = rand()%(r.sz.y - 1);
        for(p.x = 0; p.x < r.sz.x; ++p.x)
            mz.mtxSpots[r.p + p].vLinks[3].pPass->bWalled = true;

        p.x = rand()%r.sz.x;
        mz.mtxSpots[r.p + p].vLinks[3].pPass->bWalled = false;

        v.push_back(Rectangle(r.Left(), r.Top(), r.Right(), r.Top() + p.y + 1));
        v.push_back(Rectangle(r.Left(), r.Top() + p.y + 1, r.Right(), r.Bottom()));
    }
    else
    {
        Point p;
        p.x = rand()%(r.sz.x - 1);
        for(p.y = 0; p.y < r.sz.y; ++p.y)
            mz.mtxSpots[r.p + p].vLinks[1].pPass->bWalled = true;

        p.y = rand()%r.sz.y;
        mz.mtxSpots[r.p + p].vLinks[1].pPass->bWalled = false;

        v.push_back(Rectangle(r.Left(), r.Top(), r.Left() + p.x + 1, r.Bottom()));
        v.push_back(Rectangle(r.Left() + p.x + 1, r.Top(), r.Right(), r.Bottom()));
    }
}

void DivideMazeInit(SquareMaze& mz, std::list<Rectangle>& v)
{
    Point p;
    for(p.x = 0; p.x < mz.mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mz.mtxSpots.GetSize().y; ++p.y)
    for(int i = 0; i < 4; ++i)
        if(mz.mtxSpots[p].vLinks[i].pPass)
            mz.mtxSpots[p].vLinks[i].pPass->bWalled = false;

    v.push_back(mz.mtxSpots.GetSize());

}

void DivideMaze(SquareMaze& sq)
{
    std::list<Rectangle> v;
    DivideMazeInit(sq, v);

    while(DivideMazeStep(sq, v));
}

bool DivideMazeStep(SquareMaze& sq, std::list<Rectangle>& v)
{
    std::list<Rectangle> vBuff;
    while(!v.empty())
    {
        DivideMazeRec(sq, v.front(), vBuff);
        v.pop_front();
    }
    v = vBuff;
    return !v.empty();
}

void EllerMazeInit(SquareMaze& sq)
{
    Point p;
    for(p.y = 0; p.y < sq.mtxSpots.GetSize().y; ++p.y)
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        sq.mtxSpots[p].nState = -1;

    p = Point(0,0);
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        sq.mtxSpots[p].nState = p.x;
}

bool EllerMazeStep(SquareMaze& sq, int& nY)
{
    Point p(0, nY);
    if(nY < sq.mtxSpots.GetSize().y - 1)
    {
        // Horizontal passages
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            Point pPrev = Point(p.x - 1, p.y);

            if(sq.mtxSpots[p].nState != sq.mtxSpots[pPrev].nState)
                if(rand()%3)
                {
                    sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
                    int nStateFrom = sq.mtxSpots[p].nState;
                    int nStateTo = sq.mtxSpots[pPrev].nState;

                    Point q(0, p.y);
                    for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
                        if(sq.mtxSpots[q].nState == nStateFrom)
                            sq.mtxSpots[q].nState = nStateTo;
                }
        }
            
        // Vertical 'must have' passages
        std::vector<bool> vToCarve(sq.mtxSpots.GetSize().x, false);
        
        for(int id = 0; id < sq.mtxSpots.GetSize().x; ++id)
        {
            std::vector<int> vPnts;
            for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
                if(sq.mtxSpots[p].nState == id)
                    vPnts.push_back(p.x);
            
            if(vPnts.empty())
                continue;
            
            vToCarve[vPnts[rand()%vPnts.size()]] = true;
        }

        // Vertical passages
        for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            if(vToCarve[p.x] || (rand()%3 == 0))
            {
                sq.mtxSpots[p].vLinks[3].pPass->bWalled = false;
                sq.mtxSpots[Point(p.x, p.y + 1)].nState = sq.mtxSpots[p].nState;
            }
        }

        // Id assignment
        Point q = Point(0, p.y + 1);
        std::set<int> sIds;
        for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
            sIds.insert(q.x);
        for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
            sIds.erase(sq.mtxSpots[q].nState);
        for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
            if(sq.mtxSpots[q].nState == -1)
            {
                sq.mtxSpots[q].nState = *sIds.begin();
                sIds.erase(sIds.begin());
            }
    }
    else if(nY == sq.mtxSpots.GetSize().y - 1)
    {
        // final row iteration
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            Point pPrev = Point(p.x - 1, p.y);

            if(sq.mtxSpots[p].nState != sq.mtxSpots[pPrev].nState)
            {
                sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
                int nStateFrom = sq.mtxSpots[p].nState;
                int nStateTo = sq.mtxSpots[pPrev].nState;

                Point q(0, p.y);
                for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
                    if(sq.mtxSpots[q].nState == nStateFrom)
                        sq.mtxSpots[q].nState = nStateTo;
            }
        }
    }
    else
        return false;

    ++nY;
    return true;
}

void EllerMaze(SquareMaze& sq)
{
    EllerMazeInit(sq);

    int nY = 0;

    while(EllerMazeStep(sq, nY));
    
}

void BinaryMaze(SquareMaze& sq)
{
    Point p;
    for(p.y = 0; p.y < sq.mtxSpots.GetSize().y; ++p.y)
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
    {
        int n = rand()%2;
        if (((n == 0) || (p.y == 0)) && (p.x != 0))
            sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
        if (((n == 1) || (p.x == 0)) && (p.y != 0))
            sq.mtxSpots[p].vLinks[2].pPass->bWalled = false;
    }
}

bool SidewinderMazeStep(SquareMaze& sq, int& nY)
{
    Point p (0 , nY);
    if(nY > 0)
    {
        int nStart = 0;
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            if(rand()%3)
                sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
            else
            {
                int n = nStart + rand()%(p.x - nStart);
                sq.mtxSpots[Point(n, p.y)].vLinks[2].pPass->bWalled = false;
                nStart = p.x;
            }
        }
        int n = nStart + rand()%(p.x - nStart);
        sq.mtxSpots[Point(n, p.y)].vLinks[2].pPass->bWalled = false;
    }
    else if(nY == 0)
    {
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
            sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
    }
    else
        return false;

    --nY;
    return true;
}

void SidewinderMaze(SquareMaze& sq)
{
    int nY = sq.mtxSpots.GetSize().y - 1;
    while(SidewinderMazeStep(sq, nY));
}

int Openness(Spot* pSpot)
{
    int nRet = 0;

    for(size_t i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pPass && !pSpot->vLinks[i].pPass->bWalled)
            ++nRet;
    return nRet;
}

void SimpleBraidMazeInit(Maze& mz, std::vector<Wall>& vWalls, bool bWeigh)
{
    GetWalls(mz, vWalls);
    if(!bWeigh)
        Shuffle(vWalls);
    else
        ShuffleWeighed(vWalls, false);

    unsigned i;
    for(i = 0; i < mz.vSpots.size(); ++i)
        mz.vSpots[i]->nState = 0;

    for(i = 0; i < vWalls.size(); ++i)
        vWalls[i].l.pPass->bWalled = false;
}

void SimpleBraidMazeCompletionInit(Maze& mz, std::vector<Wall>& vWalls, bool bWeigh)
{
    unsigned i;

    for(i = 0; i < mz.vSpots.size(); ++i)
        mz.vSpots[i]->nState = 0;

    std::vector<Wall> vAllWalls;
    GetWalls(mz, vAllWalls);

    for(i = 0; i < vAllWalls.size(); ++i)
        if(!vAllWalls[i].l.pPass->bWalled)
            vWalls.push_back(vAllWalls[i]);

    if(!bWeigh)
        Shuffle(vWalls);
    else
        ShuffleWeighed(vWalls, false);

    for(i = 0; i < mz.vSpots.size(); ++i)
        mz.vSpots[i]->nState = 0;
}

bool SimpleBraidMazeStep(std::vector<Wall>& vWalls)
{
    if(vWalls.empty())
        return false;

    Wall kw = vWalls.back();
    vWalls.pop_back();
    
    if (Openness(kw.pSpot) <= 2)
        return true;
    if (Openness(kw.l.pTo) <= 2)
        return true;

    kw.l.pPass->bWalled = true;

    if (Distance(kw.pSpot, kw.l.pTo) == -1)
        kw.l.pPass->bWalled = false;

    return true;
}


void SimpleBraidMaze(Maze& mz, bool bWeigh)
{
    std::vector<Wall> vWalls;

    SimpleBraidMazeInit(mz, vWalls, bWeigh);

    while(SimpleBraidMazeStep(vWalls));
}
void SimpleBraidMazeCompletion(Maze& mz, bool bWeigh)
{
    std::vector<Wall> vWalls;

    SimpleBraidMazeCompletionInit(mz, vWalls, bWeigh);

    while(SimpleBraidMazeStep(vWalls));
}


bool RecursiveOpenMazeStep(std::list<Spot*>& vStack)
{
    if(vStack.empty())
        return false;
    
    Spot* pSpot = vStack.back();

    std::vector<Link> vChoices;

    for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
            vChoices.push_back(pSpot->vLinks[i]);

    if(vChoices.empty())
    {
        vStack.pop_back();

        if(Openness(pSpot) != 1)
            return true;

        std::vector<Link> vWalled;
        for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
            if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pPass->bWalled)
                vWalled.push_back(pSpot->vLinks[i]);

        if(vWalled.size())
            vWalled[RandomWalk(vWalled)].pPass->bWalled = false;

        return true;
    }

    int n = RandomWalk(vChoices);

    vChoices[n].pPass->bWalled = false;
    vStack.push_back(vChoices[n].pTo);

    return true;
}

void DeadEndRemoverInit(Maze& mz, std::vector<Spot*>& vDead, float fRemove)
{
    unsigned i;
    for(i = 0; i < mz.vSpots.size(); ++i)
        if(Openness(mz.vSpots[i]) == 1)
            vDead.push_back(mz.vSpots[i]);

    Shuffle(vDead);

    vDead.resize(unsigned(vDead.size() * fRemove));
}

bool DeadEndRemoverStep(std::vector<Spot*>& vDead)
{
    if(vDead.empty())
        return false;

    Spot* pSpot = vDead.back();
    vDead.pop_back();

    if(Openness(pSpot) != 1)
        return true;

    std::vector<Link> vWalled;
    for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pPass->bWalled)
            vWalled.push_back(pSpot->vLinks[i]);

    if(vWalled.size())
        vWalled[RandomWalk(vWalled)].pPass->bWalled = false;

    return true;
}


void DeadEndRemover(Maze& mz, float fRemove)
{
    std::vector<Spot*> vDead;

    DeadEndRemoverInit(mz, vDead, fRemove);

    while(DeadEndRemoverStep(vDead));
}

bool DeadEndRemoverTameStep(std::vector<Spot*>& vDead, bool bMin, int nMaxPath)
{
    if(vDead.empty())
        return false;

    Spot* pSpot = vDead.back();
    vDead.pop_back();

    if(Openness(pSpot) != 1)
        return true;

    Statistician<int, int> stat;
    std::vector<Link> vWalled;
    for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pPass->bWalled)
        {
            int nD = Distance(pSpot, pSpot->vLinks[i].pTo);

            if(nMaxPath && nD > nMaxPath)
                continue;

            if(bMin)
                stat.Add(nD, i);
            else
                vWalled.push_back(pSpot->vLinks[i]);
        }

    if(bMin)
    {
        if(stat.nObj)
            pSpot->vLinks[stat.oMin].pPass->bWalled = false;
    }
    else if(vWalled.size())
        vWalled[RandomWalk(vWalled)].pPass->bWalled = false;

    return true;
}

void DeadEndRemoverTame(Maze& mz, float fRemove, bool bMin, int nMaxPath)
{
    std::vector<Spot*> vDead;

    DeadEndRemoverInit(mz, vDead, fRemove);

    while(DeadEndRemoverTameStep(vDead, bMin, nMaxPath));
}

bool IsBlump(SquareMaze& sq, Point p)
{
    return
       (sq.mtxSpots.at(Point(p.x, p.y)).vLinks[1].pPass->bWalled == false) &&
       (sq.mtxSpots.at(Point(p.x, p.y)).vLinks[3].pPass->bWalled == false) &&
       (sq.mtxSpots.at(Point(p.x + 1, p.y + 1)).vLinks[0].pPass->bWalled == false) &&
       (sq.mtxSpots.at(Point(p.x + 1, p.y + 1)).vLinks[2].pPass->bWalled == false);
}

bool NoWay(Pass* p)
{
    return !p || p->bWalled;
}

void BlumpsRemover(SquareMaze& sq, bool bBraid)
{
    std::vector<Point> vBlumps;

    Point p;

    for(p.y = 0; p.y < sq.mtxSpots.GetSize().y - 1; ++p.y)
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x - 1; ++p.x)
        if(IsBlump(sq, p))
            vBlumps.push_back(p);

    Shuffle(vBlumps);

    int wall_map[5*4] = {
                            0,  0, 0, 2, 1,
                            1,  0, 2, 1, 3,
                            1,  1, 1, 3, 0,
                            0,  1, 3, 0, 2
                          };

    for(unsigned i = 0; i < vBlumps.size(); ++i)
    {
        p = vBlumps[i];
        if(!IsBlump(sq, p))
            continue;

        if(!bBraid)
        {
            int j = rand()%4;
            int j_ = (j + 1)%4;
            Point q1 = p + Point(wall_map[j*5 + 0], wall_map[j*5 + 1]);
            int nRelRight = wall_map[j*5 + 4];
            sq.mtxSpots[q1].vLinks[nRelRight].pPass->bWalled = true;
            continue;
        }

        std::vector<int> vAvSides;
        int j;
        for(j = 0; j < 4; ++j)
        {
            int j_ = (j + 1)%4;
            Point q1 = p + Point(wall_map[j*5 + 0], wall_map[j*5 + 1]);
            Point q2 = p + Point(wall_map[j_*5 + 0], wall_map[j_*5 + 1]);
            
            int nRelLeft  = wall_map[j*5 + 2];
            int nRelUp    = wall_map[j*5 + 3];
            int nRelRight = wall_map[j*5 + 4];

            if(
//               (sq.mtxSpots[q1].vLinks[nRelLeft].pPass  || sq.mtxSpots[q1].vLinks[nRelUp].pPass) &&
//               (sq.mtxSpots[q2].vLinks[nRelRight].pPass || sq.mtxSpots[q2].vLinks[nRelUp].pPass)
               sq.mtxSpots[q1].vLinks[nRelUp].pPass && sq.mtxSpots[q2].vLinks[nRelUp].pPass
              )
                vAvSides.push_back(j);
        }

        if(vAvSides.empty())
            continue;

        j = vAvSides[rand()%vAvSides.size()];

        int j_ = (j + 1)%4;
        Point q1 = p + Point(wall_map[j*5 + 0], wall_map[j*5 + 1]);
        Point q2 = p + Point(wall_map[j_*5 + 0], wall_map[j_*5 + 1]);

        int nRelLeft  = wall_map[j*5 + 2];
        int nRelUp    = wall_map[j*5 + 3];
        int nRelRight = wall_map[j*5 + 4];

        sq.mtxSpots[q1].vLinks[nRelRight].pPass->bWalled = true;

        if(NoWay(sq.mtxSpots[q1].vLinks[nRelLeft].pPass) &&
           NoWay(sq.mtxSpots[q1].vLinks[nRelUp].pPass) )
        {
            /*
            if(!sq.mtxSpots[q1].vLinks[nRelLeft].pPass)
                sq.mtxSpots[q1].vLinks[nRelUp].pPass->bWalled = false;
            else if(!sq.mtxSpots[q1].vLinks[nRelUp].pPass)
                sq.mtxSpots[q1].vLinks[nRelLeft].pPass->bWalled = false;
            else
            {
                if(rand()%2)
            */
                    sq.mtxSpots[q1].vLinks[nRelUp].pPass->bWalled = false;
            /*
                else
                    sq.mtxSpots[q1].vLinks[nRelLeft].pPass->bWalled = false;
            }
            */
        }

        if(NoWay(sq.mtxSpots[q2].vLinks[nRelRight].pPass) &&
           NoWay(sq.mtxSpots[q2].vLinks[nRelUp].pPass) )
        {
            /*
            if(!sq.mtxSpots[q2].vLinks[nRelRight].pPass)
                sq.mtxSpots[q2].vLinks[nRelUp].pPass->bWalled = false;
            else if(!sq.mtxSpots[q2].vLinks[nRelUp].pPass)
                sq.mtxSpots[q2].vLinks[nRelRight].pPass->bWalled = false;
            else
            {
                if(rand()%2)
            */
                    sq.mtxSpots[q2].vLinks[nRelUp].pPass->bWalled = false;
            /*
                else
                    sq.mtxSpots[q2].vLinks[nRelRight].pPass->bWalled = false;
            }
            */
        }

    }
}


int DeadEndCounter(Maze& mz)
{
    int nRet = 0;
    for(size_t i = 0, sz = mz.vSpots.size(); i < sz; ++i)
        if(Openness(mz.vSpots[i]) == 1)
            ++nRet;
    return nRet;
}

float River(Maze& mz)
{
    return float(DeadEndCounter(mz))/mz.vSpots.size();
}

SpotPair MaxDistance(Maze& mz, Maze& mzSeeds)
{
    Statistician<int, SpotPair> stat;

    for(size_t i = 0, sz = mzSeeds.vSpots.size(); i < sz; ++i)
    {
        mz.Flush(-1);

        std::list<Spot*> lsPath;
        mzSeeds.vSpots[i]->nState = 0;
        lsPath.push_back(mzSeeds.vSpots[i]);

        while(lsPath.size())
        {
            Spot* pNext = lsPath.front();
            lsPath.pop_front();

            for(unsigned j = 0; j < pNext->vLinks.size(); ++j)
            {
                Link lnk = pNext->vLinks[j];
                Spot* pAdj = lnk.pTo;
                if(!pAdj || lnk.pPass->bWalled || pAdj->nState != -1)
                    continue;
                pAdj->nState = pNext->nState + 1;
                lsPath.push_back(pAdj);

                stat.Add(pAdj->nState, SpotPair(mzSeeds.vSpots[i], pAdj));
            }
        }
    }

    mz.Flush(0);
    stat.oMax.n = stat.vMax;

    return stat.oMax;
}

bool FindPath(SpotPair sp, Maze& mz)
{
    mz.Flush(-1);

    std::list<Spot*> lsPath;
    sp.p1->nState = 0;
    lsPath.push_back(sp.p1);

    while(lsPath.size())
    {
        Spot* pNext = lsPath.front();
        lsPath.pop_front();

        for(unsigned j = 0; j < pNext->vLinks.size(); ++j)
        {
            Link lnk = pNext->vLinks[j];
            Spot* pAdj = lnk.pTo;
            if(!pAdj || lnk.pPass->bWalled || pAdj->nState != -1)
                continue;
            pAdj->nState = pNext->nState + 1;
            lsPath.push_back(pAdj);

            if(pAdj == sp.p2)
            {
                Spot* pCurr = pAdj;
                for(int i = pAdj->nState - 1; i >= 0; --i)
                {
                    for(unsigned k = 0; k < pCurr->vLinks.size(); ++k)
                    {
                        Link lnk = pCurr->vLinks[k];
                        Spot* pAdj = lnk.pTo;
                        if(!pAdj || lnk.pPass->bWalled)
                            continue;

                        if(pAdj->nState == i)
                        {
                            pCurr->nState = -2;
                            pCurr = pAdj;
                            break;
                        }
                    }
                }

                pCurr->nState = -2;

                for(unsigned j = 0; j < mz.vSpots.size(); ++j)
                    if(mz.vSpots[j]->nState != -2)
                        mz.vSpots[j]->nState = 0;
                    else
                        mz.vSpots[j]->nState = 1;

                return true;
            }
        }
    }

    return false;
}

int Loopiness(Maze& mz)
{
    int nLoops = 0;

    mz.Flush(-1);

    std::list<Spot*> lsPath;
    Spot* pSeed = mz.vSpots[rand()%mz.vSpots.size()];
    pSeed->nState = 0;
    lsPath.push_back(pSeed);

    while(lsPath.size())
    {
        Spot* pNext = lsPath.front();
        lsPath.pop_front();

        for(unsigned j = 0; j < pNext->vLinks.size(); ++j)
        {
            Link lnk = pNext->vLinks[j];
            Spot* pAdj = lnk.pTo;
            if(!pAdj || lnk.pPass->bWalled)
                continue;
            if(pAdj->nState != -1)
            {
                ++nLoops;
                continue;
            }
            pAdj->nState = pNext->nState + 1;
            lsPath.push_back(pAdj);
        }
    }

    return nLoops - mz.vSpots.size() + 1;
}

int MaxiLooper(Maze& mz, unsigned nSample, int nRuns)
{
    int nRet = -1;
    
    std::vector<Wall> vWalls;
    GetWalls(mz, vWalls);

    for(int nR = 0; nR < nRuns; ++nR)
    {
        mz.Flush();
        
        std::vector<Wall> vSample;
        unsigned i;
        for(i = 0; i < nSample; ++i)
        {
            Wall kw = vWalls[rand()%vWalls.size()];
            if(kw.l.pPass->bWalled)
                vSample.push_back(kw);
        }
        
        if(vSample.empty())
            continue;

        Statistician<int, int> stat;

        for(i = 0; i < vSample.size(); ++i)
            stat.Add(Distance(vSample[i].pSpot, vSample[i].l.pTo), i);
        vSample[stat.oMax].l.pPass->bWalled = false;
        nRet = stat.vMax;
    }
    return nRet;
}

int MaxiLooper(Maze& mz, int nRuns)
{
    int nRet = -1;
    
    mz.Flush();

    std::vector<Wall> vWalls;
    GetWalls(mz, vWalls);

    for(int nR = 0; nR < nRuns; ++nR)
    {
        mz.Flush();
        
        Statistician<int, int> stat;

        for(unsigned i = 0; i < vWalls.size(); ++i)
            stat.Add(Distance(vWalls[i].pSpot, vWalls[i].l.pTo), i);
        vWalls[stat.oMax].l.pPass->bWalled = false;
        nRet = stat.vMax;
    }
    return nRet;
}

void CarveMazeSpace(Maze& mz, bool bCleanUp)
{
    Maze mzPartial;
    
    for(unsigned i = 0; i < mz.vSpots.size(); ++i)
        if(!IsIsolated(mz.vSpots[i]))
            mzPartial.vSpots.push_back(mz.vSpots[i]);
        else if(bCleanUp)
        {
            unsigned j;
            for(j = 0; j < mz.vSpots[i]->vLinks.size(); ++j)
                if(IsIsolated(mz.vSpots[i]->vLinks[j].pTo))
                    break;
            if(j == mz.vSpots[i]->vLinks.size())
                mzPartial.vSpots.push_back(mz.vSpots[i]);
        }
        
    mz = mzPartial;
}

void LimitPartialMaze(Maze& mz)
{
    std::set<Spot*> stMz;

    unsigned i, j;
    for(i = 0; i < mz.vSpots.size(); ++i)
        stMz.insert(mz.vSpots[i]);

    for(i = 0; i < mz.vSpots.size(); ++i)
    {
        Spot* pSp = mz.vSpots[i];
        for(j = 0; j < pSp->vLinks.size(); ++j)
            if(pSp->vLinks[j].pTo && stMz.find(pSp->vLinks[j].pTo) == stMz.end())
            {
                pSp->vLinks[j].pTo = 0;
                pSp->vLinks[j].pPass = 0;
            }

    }
}