#include "visual.h"

void PowerFont::DrawColorWord(PowerWord s, Point p, Color c)
{
    for(int i = 0; i < s.sWord.size(); ++i)
    {
        std::string sSymb = "";
        sSymb += s.sWord[i];
        
        if(!s.IsPower(i))
        {
            if(i != 0)
            {
                if(s.IsPower(i - 1))
                {
                    p.x -= pSmallFont->nGap;
                    p.x += nGap;
                    p.y -= pPowerGap.y;
                }
            }

            pFont->DrawColorWord(sSymb, p, c);
            p.x += pFont->GetSize(sSymb).x + pFont->nGap;
        }
        else
        {
            if(i != 0)
            {
                if(!s.IsPower(i - 1))
                {
                    p.x -= pFont->nGap;
                    p += pPowerGap;
                }
            }
            else
                p.y += pPowerGap.y;

            pSmallFont->DrawColorWord(sSymb, p, c);
            p.x += pSmallFont->GetSize(sSymb).x + pSmallFont->nGap;
        }
    }
}

Point Grid::Wrap(Point p)
{
    while(p.x < 0)
        p.x += sz.x;

    while(p.x >= sz.x)
        p.x -= sz.x;

    while(p.y < 0)
        p.y += sz.y;

    while(p.y >= sz.y)
        p.y -= sz.y;

    return p;
}

Point Grid::Get(Point p)
{
    p = Wrap(p);

    return Point(pOffset.x + p.x * pGap.x, pOffset.y + p.y * pGap.y);
}

void AdjustOffset(MyCamera cc, Grid& g)
{
    g.pOffset.x = (cc.fromF(fPoint(1,1)).x - g.sz.x * g.pGap.x)/2;
    g.pOffset.y = (cc.fromF(fPoint(1,1)).y - g.sz.y * g.pGap.y)/2;
}


Index GetWallImage(SP<Graphic> pGr, MyPreloader& pr, WallDescription& wd)
{
    Index img = pGr->CopyImage(pr["tile_0"]);

    if(wd.vWalls[0])
        pGr->ImageOnto(img, Point(), pr["tile_L"], pGr->GetImage(img)->GetSize());
    if(wd.vWalls[1])
        pGr->ImageOnto(img, Point(), pr["tile_R"], pGr->GetImage(img)->GetSize());
    if(wd.vWalls[2])
        pGr->ImageOnto(img, Point(), pr["tile_B"], pGr->GetImage(img)->GetSize());
    if(wd.vWalls[3])
        pGr->ImageOnto(img, Point(), pr["tile_T"], pGr->GetImage(img)->GetSize());

    return img;
}


SP<GlobalController> GetGlobalController(ProgramEngine pe)
{
    return new AlgebraGameGlobalController(pe);
}

ProgramInfo GetProgramInfo()
{
    ProgramInfo inf;
    
    inf.szScreenRez = Size(640, 640);
    inf.strTitle = "Algebra Game";
    inf.nFramerate = 50;
    
    return inf;
}

int nOrd = 8;

Point NumToPos(int i)
{
    return Point(i % nOrd, i / nOrd);
}

int PosToNum(Point p)
{
    return p.x + p.y * nOrd;
}

ScreenObject* AlgebraGameGlobalController::GetScreenObj(Point p)
{
    for(unsigned i = 0; i < vObj.size(); ++i)
        if(vObj[i].p == p)
            return &(vObj[i]);
    return 0;
}

float Trapesoid(float fTime, float fA, float fB)
{
    if(fTime < 0)
        return 0;
    if(fTime < fA)
        return fTime / fA;
    if(fTime < fA + fB)
        return 1.F;
    if(fTime < fA + fB + fA)
        return 1 - (fTime - fA - fB)/(fA);
    return 0;
}

float Rise(float fTime, float fA, float fB)
{
    if(fTime < fA)
        return 0;
    if(fTime < fA + fB)
        return 1 - (fA + fB - fTime)/fB;
    return 1;
}

AlgebraGameGlobalController::AlgebraGameGlobalController(ProgramEngine pe):fTrans(1)
{
    ProgramInfo inf = GetProgramInfo();
    
    Rectangle sBound = Rectangle(inf.szScreenRez);
	unsigned nScale = 1;
    Rectangle rBound = Rectangle(0, 0, sBound.sz.x/nScale, sBound.sz.y/nScale);

    pGr = pe.pGr;

    pMsg = pe.pMsg;

    {
        std::ifstream ifs("config.txt");

        if(ifs.fail())
            throw SimpleException("AlgebraGameGlobalController", "<constructor>", "Need config.txt file!");

        ifs >> fp;
    }
    

    pSndMng = pe.pSndMng;

    pPr = new MyPreloader(pGr, pSndMng, fp);
    MyPreloader& pr = *pPr;

    pr.AddTransparent(Color(0,0,0));
    pr.SetScale(nScale);

    pr.LoadTS("AlgebraGuy.bmp", "sel", Color(255,255,255));

    pr.LoadTS("NewTile0.bmp", "tile_0", Color(1, 1, 1)); 
    pr.LoadTS("NewTileL.bmp", "tile_L", Color(255, 255, 255)); 
    pr.LoadTS("NewTileR.bmp", "tile_R", Color(255, 255, 255)); 
    pr.LoadTS("NewTileT.bmp", "tile_T", Color(255, 255, 255)); 
    pr.LoadTS("NewTileB.bmp", "tile_B", Color(255, 255, 255)); 
    
    cc = MyCamera(pGr, Scale(float(nScale), rBound));

    pFont = new FontWriter(fp, "font.txt", pGr, nScale*2);
    pFont->SetGap(-10 * nScale);

    pSmallFont = new FontWriter(fp, "font.txt", pGr, nScale);
    pSmallFont->SetGap(-2 * nScale);

    pf.pFont = pFont;
    pf.pSmallFont = pSmallFont;
    pf.pPowerGap = Point(-3, 1);
    pf.nGap = 0;

    g.sz = Size(nOrd, nOrd);
    g.pGap = cc.fromR(Point(35,35));

    SquareMaze sq(Size(nOrd, nOrd));
    Maze mz;
    sq.GetMaze(mz);

    KruskalMaze(mz);

    mz.Flush(0);

    Point p;
    for(p.x = 0; p.x < nOrd/2; ++p.x)
    for(p.y = 0; p.y < nOrd/2; ++p.y)
    {
        Spot* pSpot = &sq.mtxSpots[p];
        for(int i = 0; i < 4; ++i)
            if(pSpot->vLinks[i].pPass)
                pSpot->vLinks[i].pPass->bWalled = true;
    }

    Maze mzSeed;

    for(int i = 0; i < nOrd/2; ++i)
    {
        mzSeed.vSpots.push_back(&sq.mtxSpots[Point(nOrd/2, i)]);
        mzSeed.vSpots.push_back(&sq.mtxSpots[Point(i, nOrd/2)]);
    }
    mzSeed.vSpots.push_back(&sq.mtxSpots[Point(nOrd/2, nOrd/2)]);

    SpotPair sp = MaxDistance(mz, mzSeed);
    FindPath(sp, mz);

    for(p.x = 0; p.x < nOrd; ++p.x)
    for(p.y = 0; p.y < nOrd; ++p.y)
    {
        Spot* pSpot = &sq.mtxSpots[p];
        if(pSpot->nState == 1)
            continue;
        for(int i = 0; i < 4; ++i)
            if(pSpot->vLinks[i].pPass)
                pSpot->vLinks[i].pPass->bWalled = true;
    }

    sp.p1->nState = 2;
    sp.p2->nState = 2;

    SquareMaze sq_little(Size(nOrd/2, nOrd/2));
    sq_little.GetMaze(mz);
    RecursiveMaze(mz);

    Group g1;
    Group g2;

    DihedralGroup(nOrd/2, g1);
    DihedralGroup(nOrd/2, g2);
    
    CrossProduct(grp, g1, g2);

    /*
    std::vector<int> vElem;
    vElem.push_back(1);
    vElem.push_back(4);
    vElem.push_back(2);
    vElem.push_back(3);
    MultiplicativeGroup mg(nOrd, vElem);

    SemiDirectProduct(grp, mg);
    */

    for(p.y = 0; p.y < g.sz.y/2; ++p.y)
    for(p.x = 0; p.x < g.sz.x/2; ++p.x)
    {

        ScreenObject obj;

        Spot* pSpot = &sq_little.mtxSpots[p];

        obj.wd.vWalls[0] = pSpot->vLinks[0].pPass ? pSpot->vLinks[0].pPass->bWalled : true;
        obj.wd.vWalls[1] = pSpot->vLinks[1].pPass ? pSpot->vLinks[1].pPass->bWalled : true;
        obj.wd.vWalls[2] = pSpot->vLinks[3].pPass ? pSpot->vLinks[3].pPass->bWalled : true;
        obj.wd.vWalls[3] = pSpot->vLinks[2].pPass ? pSpot->vLinks[2].pPass->bWalled : true;

        if(sq.mtxSpots[Point(p.x + 1, p.y)].nState == 2)
        {
            sq.mtxSpots[Point(p.x + 1, p.y)].vLinks[0].pPass->bWalled = false;
            obj.wd.vWalls[1] = false;
        }
        if(sq.mtxSpots[Point(p.x, p.y + 1)].nState == 2)
        {
            sq.mtxSpots[Point(p.x, p.y + 1)].vLinks[2].pPass->bWalled = false;
            obj.wd.vWalls[2] = false;
        }


        obj.iImg = GetWallImage(pGr, pr, obj.wd);//pr["grid"];
        obj.p = p;
        obj.nGrpEl = PosToNum(p);
        obj.sText = S(obj.nGrpEl);

        vObj.push_back(obj);
    }

    for(p.y = 0; p.y < g.sz.y; ++p.y)
    for(p.x = 0; p.x < g.sz.x; ++p.x)
    {

        if(p.y < g.sz.y/2 && p.x < g.sz.x/2)
            continue;

        ScreenObject obj;

        Spot* pSpot = &sq.mtxSpots[p];

        obj.wd.vWalls[0] = pSpot->vLinks[0].pPass ? pSpot->vLinks[0].pPass->bWalled : true;
        obj.wd.vWalls[1] = pSpot->vLinks[1].pPass ? pSpot->vLinks[1].pPass->bWalled : true;
        obj.wd.vWalls[2] = pSpot->vLinks[3].pPass ? pSpot->vLinks[3].pPass->bWalled : true;
        obj.wd.vWalls[3] = pSpot->vLinks[2].pPass ? pSpot->vLinks[2].pPass->bWalled : true;

        obj.iImg = GetWallImage(pGr, pr, obj.wd);//pr["grid"];
        obj.p = p;
        obj.nGrpEl = PosToNum(p);
        obj.sText = S(obj.nGrpEl);

        vObj.push_back(obj);
    }

    pSel = Point(0, 0);

    
    Point p_rnd (rand()%(nOrd/2), rand()%(nOrd/2));
    int n = PosToNum(p_rnd);

    for(unsigned i = 0; i < vObj.size(); ++i)
    {
        vObj[i].nGrpEl = grp.Mult(grp.Mult(n, vObj[i].nGrpEl), grp.Inv(n));
        vObj[i].pStr = vObj[i].p;
        vObj[i].p = NumToPos(vObj[i].nGrpEl);
    }
}

void AlgebraGameGlobalController::Update()
{
    pGr->DrawRectangle(GetProgramInfo().szScreenRez, Color(0,255,255), false);
    
    Point pg = g.pGap;

    g.pGap.x = g.pGap.x * (1+Trapesoid(fTrans, .25, .5));
    g.pGap.y = g.pGap.y * (1+Trapesoid(fTrans, .25, .5));
    
    AdjustOffset(cc, g);

    
    for(unsigned i = 0; i < vObj.size(); ++i)
    {
        Point p = g.Get(vObj[i].p);

        if(fTrans != 1)
        {
            float f_adj = Rise(fTrans, .25, .5);
            
            p = (fPoint(g.Get(vObj[i].pStr)) * (1.F - f_adj) + fPoint(g.Get(vObj[i].p)) * f_adj).ToPnt();
        }

        cc.DrawImage(p, vObj[i].iImg, true);
        //pFont->DrawWord(vObj[i].sText, cc.toR(p), true);
    }

    if(fTrans != 1)
    {
        fTrans += .03F;

        if(fTrans >= 1)
            fTrans = 1;
    }

    pSel = g.Wrap(pSel);
    cc.DrawImage(g.Get(pSel), (*pPr)["sel"], true);

    g.pGap = pg;

    pGr->RefreshAll();
}

void AlgebraGameGlobalController::KeyDown(GuiKeyType nCode)
{
    Point pSelOld = pSel;
    
    if(nCode == GUI_UP)
    {
        pSel += Point(0, -1);
    }
    if(nCode == GUI_DOWN)
    {
        pSel += Point(0, 1);
    }
    if(nCode == GUI_LEFT)
    {
        pSel += Point(-1, 0);
    }
    if(nCode == GUI_RIGHT)
    {
        pSel += Point(1, 0);
    }

    pSel = g.Wrap(pSel);

    if(pSel != pSelOld)
    {
        ScreenObject* pObjOld = GetScreenObj(pSelOld);
        ScreenObject* pObj = GetScreenObj(pSel);

        bool bWall = false;

        if(nCode == GUI_UP)
            bWall = pObj->wd.vWalls[2] | pObjOld->wd.vWalls[3];
        else if(nCode == GUI_DOWN)
            bWall = pObj->wd.vWalls[3] | pObjOld->wd.vWalls[2];
        else if(nCode == GUI_LEFT)
            bWall = pObj->wd.vWalls[1] | pObjOld->wd.vWalls[0];
        else if(nCode == GUI_RIGHT)
            bWall = pObj->wd.vWalls[0] | pObjOld->wd.vWalls[1];

        if(bWall)
            pSel = pSelOld;
    }


    if(nCode == 'a')
    {
        int n = PosToNum(pSel);

        for(unsigned i = 0; i < vObj.size(); ++i)
        {
            vObj[i].nGrpEl = grp.Mult(n, vObj[i].nGrpEl);
            vObj[i].pStr = vObj[i].p;
            vObj[i].p = NumToPos(vObj[i].nGrpEl);
        }

        fTrans = 0;
    }

    if(nCode == 's')
    {
        int n = PosToNum(pSel);

        for(unsigned i = 0; i < vObj.size(); ++i)
        {
            vObj[i].nGrpEl = grp.Mult(grp.Mult(n, vObj[i].nGrpEl), grp.Inv(n));
            vObj[i].pStr = vObj[i].p;
            vObj[i].p = NumToPos(vObj[i].nGrpEl);
        }

        fTrans = 0;
    }

    if(nCode == 'd')
    {
        int n = PosToNum(pSel);

        for(unsigned i = 0; i < vObj.size(); ++i)
        {
            vObj[i].nGrpEl = grp.Mult(vObj[i].nGrpEl, n);
            vObj[i].pStr = vObj[i].p;
            vObj[i].p = NumToPos(vObj[i].nGrpEl);
        }

        fTrans = 0;
    }

    if(nCode == 'q')
    {
        for(unsigned i = 0; i < vObj.size(); ++i)
        {
            vObj[i].nGrpEl = i;
            vObj[i].p = NumToPos(vObj[i].nGrpEl);
        }
    }
}
