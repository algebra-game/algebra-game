#include <iostream>
#include <vector>
#include <set>
#include <map>

struct Group
{
    typedef std::vector<int> LeftMult;
    std::vector<LeftMult> vElements;

    int Mult(int a, int b) const 
    {
        return vElements[a][b];
    }

    int Inv(int a)
    {
        for(unsigned i = 0; i < vElements.size(); ++i)
            if(vElements[i][a] == 0)
                return i;
        throw std::string("no inverse");
    }
};

void CyclicGroup(int n, Group& g);

void DihedralGroup(int n, Group& g);

void CrossProduct(Group& g, const Group& h1, const Group& h2);

struct MultiplicativeGroup: public Group
{
    int nMod;
    std::vector<int> vElem;

    MultiplicativeGroup(int nMod_, const std::vector<int>& vElem_)
        :nMod(nMod_), vElem(vElem_)
    {
        InitGroup();
    }

    void InitGroup()
    {
        int n = vElem.size();

        vElements.clear();
        vElements.resize(n, Group::LeftMult(n, 0));

        std::map<int, int> mpEl;

        for(unsigned k = 0; k < vElem.size(); ++k)
            mpEl[vElem[k]] = k;
        
        for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
            vElements[i][j] = mpEl[(vElem[i]*vElem[j])%nMod];
    }

    void Generate()
    {
        std::set<int> sElemsSoFar;

        for(unsigned i = 0; i < vElem.size(); ++i)
            sElemsSoFar.insert(vElem[i]);

        for(unsigned i = 0; i < vElem.size(); ++i)
        for(unsigned j = 0; j <= i; ++j)
        {
            int n = (vElem[i]*vElem[j])%nMod;
            if(sElemsSoFar.insert(n).second == true)
                vElem.push_back(n);
        }

        InitGroup();
    }

    int Action(int i, int j)
    {
        return (vElem[i]*j)%nMod;
    }
};

void SemiDirectProduct(Group& g, MultiplicativeGroup& mg);