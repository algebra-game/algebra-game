#include "algebra.h"

void CyclicGroup(int n, Group& g)
{
    g.vElements.clear();
    g.vElements.resize(n, Group::LeftMult(n, 0));

    for(int i = 0; i < n; ++i)
    for(int j = 0; j < n; ++j)
        g.vElements[i][j] = (i + j)%n;
}

void DihedralGroup(int n, Group& g)
{
    g.vElements.clear();
    g.vElements.resize(2*n, Group::LeftMult(2*n, 0));

    for(int i = 0; i < 2*n; ++i)
    for(int j = 0; j < 2*n; ++j)
    {
        bool s1 = (i >= n);
        int  p1 = i%n;

        bool s2 = (j >= n);
        int  p2 = j%n;

        bool sr = (s1 != s2);
        //bool sr = s1 ^ s2;
        int pr;

        if(!s2)
            pr = p2 + p1;
        else
            pr = p2 - p1;

        pr += n;
        pr %= n;

        g.vElements[i][j] = (sr ? n : 0) + pr;
    }
}

void CrossProduct(Group& g, const Group& h1, const Group& h2)
{
    int n = h1.vElements.size();
    int m = h2.vElements.size();

    g.vElements.clear();
    g.vElements.resize(n*m, Group::LeftMult(n*m, 0));


    for(int i = 0; i < n*m; ++i)
    for(int j = 0; j < n*m; ++j)
        g.vElements[i][j] = h1.Mult(i%m, j%m) + h2.Mult((i - i%m)/m, (j - j%m)/m) * m;
}

void SemiDirectProduct(Group& g, MultiplicativeGroup& mg)
{    
    int n = mg.nMod;
    int m = mg.vElements.size();
    
    g.vElements.clear();
    g.vElements.resize(n*m, Group::LeftMult(n*m, 0));


    for(int i = 0; i < n*m; ++i)
    for(int j = 0; j < n*m; ++j)
    {
        int g1 = i%n;
        int g2 = j%n;

        int h1 = i/n;
        int h2 = j/n;

        int g_res = (g1 + mg.Action(h1, g2))%n;
        int h_res = mg.Mult(h1, h2);

        g.vElements[i][j] = g_res + h_res * n;
    }
}